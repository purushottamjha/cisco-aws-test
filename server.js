/**
 * @Author: Purushottam Jha
 */
/* Load dependency modules */
var express = require('express');
var port     = process.env.PORT || 3000;
var passport = require('passport');
var flash    = require('connect-flash');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');

/* Create an express app */
var app = express();

// pass passport for configuration
require('./config/passport')(passport);

// Host static files e.g. Angular js UI.
app.use(express.static('client'));

// Set up our express application
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));

// Required for passport
app.use(session({ secret: 'youshouldhirepurushottamjha' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// routes ======================================================================
require('./routes/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);

