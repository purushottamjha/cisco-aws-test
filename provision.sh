#!/usr/bin/env bash

echo "### START PROVISIONING ###"

echo "Self update apt-get"
apt-get update

echo "Installing git"
apt-get install -y git

echo "Installing curl"
apt-get install -y curl

echo "Installing node and npm"
apt-get install -y python-software-properties python g++ make
add-apt-repository ppa:chris-lea/node.js
apt-get update
apt-get install -y nodejs
npm install -g bower
npm install -g gulp

echo "cd /cisco-aws-test" >> /home/vagrant/.bashrc

