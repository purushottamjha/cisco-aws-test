/**
 * Created by purushottamjha on 7/9/15.
 */

var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');

// Gulp task to compile less files.
gulp.task('less', function () {
    return gulp.src('./less/**/*.less')
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest('./css'));
});