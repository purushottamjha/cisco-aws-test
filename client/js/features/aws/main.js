/**
 * @Author: Purushottam Jha
 */
(function (define) {
    'use strict';

    define([
        'angular',
        'features/aws/router/Routes',
        'features/aws/controller/AwsController'
    ], function (angular,
                 Routes,
                 AwsController) {

        var moduleName = 'awsgrid';

        // Create awsgrid module.
        var module = angular.module(moduleName, []);

        // Add controller to the module.
        module.controller('AwsController', AwsController);

        //return the module name which will be used as dependency in framework
        return {
            name: moduleName,
            routes: Routes
        };
    });

}(define));