/**
 * @Author: Purushottam Jha
 */
(function(define, require) {
    'use strict';

    var features = require.toUrl('features');

    // Setup the /awsdata route. Define controller and template for the route.
    define([], function() {
        return [{
            isDefault: true,
            when: '/awsdata',
            controller: 'AwsController',
            templateUrl: features + '/aws/partials/template.html'
        }];
    });

}(define, require));