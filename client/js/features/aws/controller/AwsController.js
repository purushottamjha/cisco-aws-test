/**
 * @author : Purushottam Jha
 */
(function(define) {
    'use strict';

    define([], function() {

        /**
         * Controller to fetch the aws instances data from the backend for the grid.
         * @param $scope
         * @param $http
         * @param $location
         * @constructor
         */
        var AwsController = function($scope, $http, $location) {

            //var awsApiUrl = $location.protocol()+"://"+$location.host()+":/api/awsdata"

            $scope.title = 'Aws Instances';

            var awsGrid = document.getElementById("themed");

            $http.get("/api/awsdata")
                .success(function(response) {
                    // The user is authorised and logged in, put a variable in
                    // the local storage to identify that
                    localStorage.setItem("loggedin", 'true');

                    // Data is fetched from the backend, so remove the loading spinner.
                    var element = document.getElementById("loader");
                    element.parentNode.removeChild(element);

                    // Add the data to the grid.
                    awsGrid.data = response;
                }).
                error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    // If unauthorised return to the login page.
                    if(status === 401){
                    $location.path( "/login" );
                    }
                });
        };

        return ['$scope','$http','$location', AwsController];

    });

})(define);