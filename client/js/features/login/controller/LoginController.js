/**
 * @Author: Purushottam Jha
 */
(function(define) {
    'use strict';

    define([], function() {

        /**
         * Login Controller. Handles user login.
         * @param $scope
         * @param $location
         * @constructor
         */
        var LoginController = function($scope,$location) {

            // If user is logged in, show him the awsgrid.
            if(localStorage.getItem("loggedin")){
                $location.path( "/awsdata" );
            };

        };

        return ['$scope','$location', LoginController];

    });

})(define);