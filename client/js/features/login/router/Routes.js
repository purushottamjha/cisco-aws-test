/**
 * @Author: Purushottam Jha
 */
(function(define, require) {
    'use strict';

    var features = require.toUrl('features');

    // Set up the /login route. Define controller and template for the login
    // route
    define([], function() {
        return [{
            isDefault: true,
            when: '/login',
            controller: 'LoginController',
            templateUrl: features + '/login/partials/template.html'
        }];
    });

}(define, require));