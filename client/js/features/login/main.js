/**
 * @Author: Purushottam Jha
 */
(function (define) {
    'use strict';

    define([
        'angular',
        'features/login/router/Routes',
        'features/login/controller/LoginController'
    ], function (angular,
                 Routes,
                 LoginController) {

        var moduleName = 'login';

        // Create login module.
        var module = angular.module(moduleName, []);

        // Add controller to the module.
        module.controller('LoginController', LoginController);

        //return the module name which will be used as dependency in framework
        return {
            name: moduleName,
            routes: Routes
        };
    });

}(define));