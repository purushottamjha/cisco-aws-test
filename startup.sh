#!/usr/bin/env bash

cd /cisco-aws-test
cd client
npm install --no-bin-links
bower install
gulp less

cd ..

echo "Installing Project Dependencies"
npm install --no-bin-links

echo "Starting Development Server Env"
nohup npm start 0<&- &>/dev/null &