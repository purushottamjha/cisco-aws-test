/**
 * @Author: Purushottam Jha
 * @param app
 * @param passport
 */
module.exports = function (app, passport) {

    var AWS = require('aws-sdk');
    var express = require('express');

    /**
     *  Login Authentication
     */
    // process the login form
    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/#/awsdata', // redirect to the awsdata grid
        failureRedirect: '/#/login', // redirect back to the login page if there is an error
        failureFlash: true // allow flash messages
    }));

    /* If you want to load the credentials from a property file */
    /*
     var PropertiesReader = require('properties-reader');
     var properties = PropertiesReader('./config/AwsCredentials.properties');
     var awsAccessKeyId = properties.get('aws_access_key_id');
     var awsSecretAccessKey = properties.get('aws_secret_access_key');*/

    /* I am loading the credentials from a json file */
    AWS.config.loadFromPath('./config/AwsCredentials.json');

    /* Set the region */
    AWS.config.region = 'us-west-2';

    /* Create an EC2 instance */
    var ec2 = new AWS.EC2();

    app.use('/api/awsdata', isLoggedIn);

    /**
     *  Fetch aws data
     */
    app.get('/api/awsdata', function (req, res) {
        ec2.describeInstances(function (error, data) {
            if (error) {
                console.log(error); // an error occurred
            } else {

                var reservations = data.Reservations;
                var activeInstances = [];
                for (var i = 0; i < reservations.length; i++) {

                    var instances = data.Reservations[i].Instances;

                    for (var j = 0; j < instances.length; j++) {
                        if (instances[j].State.Name == 'running') {
                            var instance = instances[j];
                            var gridData = {};
                            gridData.InstanceId = instance.InstanceId;
                            gridData.InstanceType = instance.InstanceType;
                            gridData.State = instance.State.Name;
                            gridData.AvailabilityZone = instance.Placement.AvailabilityZone;
                            gridData.PublicIpAddress = instance.PublicIpAddress;
                            gridData.PrivateIpAddress = instance.PrivateIpAddress;
                            activeInstances.push(gridData);
                        }
                    }
                }

                res.send(activeInstances);
            }
        });
    });
};

/**
 * route middleware to ensure user is logged in
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    // Is the user is not authenticated, send unauthorised error code.
    res.send(401);
}
