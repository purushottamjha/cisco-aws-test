# Usage
1. Install node
2. npm install -g bower
3. npm install -g gulp
4. cd cisco-aws-test
5. npm install
6. cd client
7. npm install
8. bower install
9. gulp less
10. cd ..
11. npm start
12. Fire up the browser at http://localhost:3000/

# If you are using vagrant
1. Make sure you have vagrant and virtual Box installed
2. cd cisco-aws-test
3. vagrant up (run 'vagrant ssh' to get into varant machine if you want.).
4. Fire up the browser at http://localhost:3000/

# Aws keys
1. Modify AwsCredentials.json file to use your accessKeyId and secretAccessKey

# Test username and password
1. Email: test@test.com
2. Password: test

# Additional Info
1. Please use Chrome. I have used polymer and shadow DOMs are not very well 
   supported in other browsers yet. Webcomponentjs has errors on other browsers.
   I have logged a complaint with Google.