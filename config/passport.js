/**
 * @Author: Purushottam Jha
 * @type {*|Strategy}
 */
// load all the things we need
var LocalStrategy    = require('passport-local').Strategy;

module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    passport.serializeUser(function(user, cb) {
        cb(null, user.email);
    });

    passport.deserializeUser(function(email, cb) {
        cb(null, USER);
    });

    // Test user so don't need database
    var USER = {
        email: 'test@test.com',
        validPassword: function(passwd) {
            return passwd === 'test';
        }
    };

    var getUser = function(email, cb) {
        if (email !== 'test@test.com')
            return cb(null, null);
        cb(null, USER);
    };

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function(req, email, password, done) {
        if (email)
            email = email.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching

        // asynchronous
        process.nextTick(function() {
            getUser(email, function(err, user) {
                // if there are any errors, return the error
                if (err)
                    return done(err);

                // if no user is found, return the message
                if (!user)
                    return done(null, false, req.flash('loginMessage', 'No user found.'));

                if (!user.validPassword(password))
                    return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));

                // all is well, return user
                else
                    return done(null, user);
            });
        });

    }));
};

